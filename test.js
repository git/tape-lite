// SPDX-FileCopyrightText: 2023 Haelwenn (lanodan) Monnier <contact+tape-lite@hacktivis.me>
// SPDX-License-Identifier: MIT

var test = require('tape-lite');

// no name
test(function(t) { t.equal(typeof Date.now, 'function'); });

test('name + callback', function(t) { t.equal(typeof Date.now, 'function'); });

test('name + opts + callback',
     {skip : false},
     function(t) { t.equal(typeof Date.now, 'function'); });

/*
test('test using promises', async function (t) {
    const result = await someAsyncThing();
    t.ok(result);
});
*/
