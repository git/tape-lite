// SPDX-FileCopyrightText: 2023 Haelwenn (lanodan) Monnier <contact+tape-lite@hacktivis.me>
// SPDX-License-Identifier: MIT

'use strict';

const test = require('node:test');

function notOk(value, msg) { return assert.ok(!value, msg); };

var onFailure = [];

// test([name], [opts], cb)
module.exports = function(name, opts, cb) {
	function setup_cb(t)
	{
		const assert = require('node:assert/strict');

		// t.plan(n)
		t.plan = (n) => console.log("t.plan called with:", n);

		// t.end(err)
		t.end = (err) => console.log("t.end called with:", err);

		// t.teardown(cb) → after([fn][, options])
		t.teardown = test.after;

		// t.fail(msg)
		t.fail = assert.fail;

		// t.pass(msg) → context.skip([message])
		// technically not the same thing but close enough
		t.pass = t.skip;

		// t.timeoutAfter(ms)

		// t.skip(msg) → context.skip([message])

		// t.ok(value, msg) → assert.ok(value[, message])
		// Aliases: t.true(), t.assert()
		t.ok     = assert.ok;
		t.true   = t.ok;
		t.assert = t.ok;

		// t.notOk(value, msg) → assert.ok(!value[, message])
		// Aliases: t.false(), t.notok()
		t.notOk = notOk;
		t.false = t.notOk;
		t.notok = t.notOk;

		// t.error(err, msg) → assert.ifError(value)
		// Aliases: t.ifError(), t.ifErr(), t.iferror()
		t.error = (err, msg) => assert.ifError(err);
		t.ifError             = t.error;
		t.ifErr               = t.error;
		t.iferror             = t.error;

		// t.equal(actual, expected, msg) → assert.equal(actual, expected[, message])
		// Aliases: t.equals(), t.isEqual(), t.strictEqual(), t.strictEquals(), t.is()
		t.equal        = assert.strictEqual;
		t.equals       = t.equal;
		t.isEqual      = t.equal;
		t.strictEqual  = t.equal;
		t.strictEquals = t.equal;
		t.is           = t.equal;

		// t.notEqual(actual, expected, msg) → assert.notStrictEqual(actual, expected[, message])
		// Aliases: t.notEquals(), t.isNotEqual(), t.doesNotEqual(), t.isInequal(), t.notStrictEqual(), t.notStrictEquals(), t.isNot(), t.not()
		t.notEqual        = assert.notStrictEqual;
		t.notEquals       = t.notEqual;
		t.doesNotEqual    = t.notEqual;
		t.isInequal       = t.notEqual;
		t.notStrictEqual  = t.notEqual;
		t.notStrictEquals = t.notEqual;
		t.isNot           = t.notEqual;
		t.not             = t.notEqual;

		// t.looseEqual(actual, expected, msg) → assert.Equal(actual, expected[, message])
		// Aliases: t.looseEquals()
		t.looseEqual  = assert.Equal;
		t.looseEquals = t.looseEqual;

		// t.notLooseEqual(actual, expected, msg) → assert.notEqual(actual, expected[, message])
		// Aliases: t.notLooseEquals()
		t.notLooseEqual  = assert.notEqual;
		t.notLooseEquals = t.notLooseEqual;

		// t.deepEqual(actual, expected, msg) → assert.deepStrictEqual(actual, expected[, message])
		// Aliases: t.deepEquals(), t.isEquivalent(), t.same()
		t.deepEqual    = assert.deepStrictEqual;
		t.deepEquals   = t.deepEqual;
		t.isEquivalent = t.deepEqual;
		t.same         = t.deepEqual;

		// t.notDeepEqual(actual, expected, msg) → assert.notDeepStrictEqual(actual, expected[, message])
		// Aliases: t.notDeepEquals, t.notEquivalent(), t.notDeeply(), t.notSame(), t.isNotDeepEqual(), t.isNotDeeply(), t.isNotEquivalent(), t.isInequivalent()
		t.notDeepEqual    = assert.notDeepStrictEqual;
		t.notDeepEquals   = t.notDeepEqual;
		t.notEquivalent   = t.notDeepEqual;
		t.notDeeply       = t.notDeepEqual;
		t.notSame         = t.notDeepEqual;
		t.isNotDeepEqual  = t.notDeepEqual;
		t.isNotDeeply     = t.notDeepEqual;
		t.isNotEquivalent = t.notDeepEqual;
		t.isInequivalent  = t.notDeepEqual;

		// t.deepLooseEqual(actual, expected, msg) → assert.deepEqual(actual, expected[, message])
		t.deepLooseEqual = assert.deepEqual;

		// t.notDeepLooseEqual(actual, expected, msg) → assert.notDeepEqual(actual, expected[, message])
		// Aliases: t.notLooseEqual(), t.notLooseEquals()
		t.notDeepLooseEqual = assert.notDeepEqual;
		t.notLooseEqual     = t.notDeepLooseEqual;
		t.notLooseEquals    = t.notDeepLooseEqual;

		// t.throws(fn, expected, msg) → assert.throws(fn[, error][, message])
		t.throws = assert.throws;

		// t.doesNotThrow(fn, expected, msg)

		// t.test(name, [opts], cb)

		// t.comment(message)
		t.comment = (msg) => console.log("t.comment called with:", msg);

		// t.match(string, regexp, message) → assert.match(string, regexp[, message])
		t.match = assert.match;

		// t.doesNotMatch(string, regexp, message) → assert.doesNotMatch(string, regexp[, message])
		t.doesNotMatch = assert.doesNotMatch;

		// npm:tap compatibility
		t.strictSame = t.same;
	};

	switch(arguments.length)
	{
	case 1:
		test((t) => {
			setup_cb(t);
			arguments[0](t)
		});
		break;
	case 2:
		test(arguments[0], (t) => {
			setup_cb(t);
			arguments[1](t)
		});
		break;
	case 3:
		test(arguments[0], arguments[1], (t) => {
			setup_cb(t);
			arguments[2](t)
		});
		break;
	default:
		abort();
	}
};

// test.skip([name], [opts], cb)

// test.onFinish(fn)
module.exports.onFinish = test.after;

// test.onFailure(fn)
module.exports.onFailure = (fn) => {
	onFailure.push(fn);
	// TODO: Call each on event:fail
};

// var htest = test.createHarness()

// test.only([name], [opts], cb)

// var stream = test.createStream(opts)
