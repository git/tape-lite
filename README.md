# tape-lite

A `tape` shim based on `node:test`+`node:assert` with no dependencies.

## Note for packagers

You can use this library as an explicit override, see <https://docs.npmjs.com/cli/v10/configuring-npm/package-json/#overrides> for documentation.

<!--
SPDX-FileCopyrightText: 2023 Haelwenn (lanodan) Monnier <contact+tape-lite@hacktivis.me>
SPDX-License-Identifier: MIT
-->
